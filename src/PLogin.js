import React, {Component, useEffect} from 'react';

import {
  View,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {PermissionCtx} from './permission.context';

let account = [
  {
    username: 'managercuf',
    password: '1234',
    position: 'manager',
    auth: 'manager',
  },
  {
    username: 'usercuf',
    password: '1234',
    position: 'user',
    auth: 'user',
  },
];

function PLogin(props) {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  useEffect(() => {
    getAuth()
      .then(async data => {
        let position = await data.position;
        let auth = await data.auth;
        if (position !== null && auth !== null) {
          props.setPosition(position);
          for (let i = 0; i < account.length; i++) {
            if (account[i].auth === auth) {
              props.setPermission(account[i].permission);
              break;
            }
          }
          props.navigation.navigate('PHome');
        }
      })
      .catch(error => {
        console.log(error);
      });
  });
  getAuth = () => {
    return new Promise((resolve, reject) => {
      try {
        let auth = AsyncStorage.getItem('auth');
        let position = AsyncStorage.getItem('position');
        resolve({position: position, auth: auth});
      } catch (error) {
        console.error(error);
        reject(error);
      }
    });
  };
  const value = React.useContext(PermissionCtx);
  checkLogin = async () => {
    try {
      let isUser = false;
      for (let i = 0; i < account.length; i++) {
        if (
          username === account[i].username &&
          password === account[i].password
        ) {
          isUser = !isUser;

          await setAuth(account[i].position, account[i].auth);
          value.fetch();
          props.setPosition(account[i].position);
          props.setPermission(account[i].permission);
          props.navigation.navigate('PHome', {
            position: account[i].position,
          });
          break;
        }
      }
      if (!isUser) {
        alert('Incorrect password');
      }
    } catch (error) {
      console.error(error);
    }
  };

  setAuth = (position, auth) => {
    return new Promise((resolve, reject) => {
      try {
        AsyncStorage.setItem('auth', auth);
        AsyncStorage.setItem('position', position);
        resolve();
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{width: '60%'}}>
        <TextInput
          style={styles.textInput}
          placeholder="username"
          placeholderTextColor="#9E9E9E"
          autoCapitalize="none"
          onChangeText={username => {
            setUsername(username);
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder="password"
          placeholderTextColor="#9E9E9E"
          autoCapitalize="none"
          onChangeText={password => {
            setPassword(password);
          }}
          secureTextEntry={true}
        />
        <TouchableOpacity style={styles.btLogin} onPress={checkLogin}>
          <Text style={styles.textLogin}>login</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const mapStateToProps = state => {
  return {
    position: state.position,
    permission: state.permission,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setPosition: position => {
      dispatch({type: 'POSITION', position: position});
    },
    setPermission: permission => {
      dispatch({type: 'PERMISSION', permission: permission});
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PLogin);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#7E3FF2',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    backgroundColor: '#ffffff',
    color: '#000000',
    height: 36,
    marginBottom: 20,
    textAlign: 'center',
    borderRadius: 50,
    fontSize: 14,
  },
  btLogin: {
    backgroundColor: '#03A9F4',
    height: 36,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLogin: {
    color: '#ffffff',
  },
});
