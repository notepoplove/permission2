import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Button} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Can} from './Can';

function PHome(props) {
  logout = async () => {
    try {
      await AsyncStorage.removeItem('auth');
      props.navigation.navigate('PLogin');
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>
        Home:
        {props.position}
      </Text>
      <TouchableOpacity style={styles.btLogout} onPress={logout}>
        <Text>logout</Text>
      </TouchableOpacity>
      {/* access ส่งไปเป็น access เริ่มต้นที่ access ที่มาจาก server ต้องมี*/}
      <Can objectRef="btEdit" access={['view']}>
        <TouchableOpacity style={fcStyles.edit}>
          <Text>edit</Text>
        </TouchableOpacity>
      </Can>
      <Can objectRef="btDelete" access={['edit']}>
        <TouchableOpacity style={fcStyles.delete}>
          <Text>delete</Text>
        </TouchableOpacity>
      </Can>
    </View>
  );
}

export default PHome;

const styles = StyleSheet.create({
  btLogout: {
    backgroundColor: '#03A9F4',
    padding: 20,
  },
});

const fcStyles = StyleSheet.create({
  edit: {
    backgroundColor: 'gray',
    padding: 20,
    marginTop: 8,
  },
  delete: {
    backgroundColor: 'red',
    padding: 20,
    marginTop: 8,
  },
});
