import React, {useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

const usePermission = function() {
  const [permission, setPermission] = React.useState();

  useEffect(() => {
    fetch();
  }, []);

  const fetch = () => {
    getAuth()
      .then(async data => {
        let position = await data.position;
        if (position === 'user') {
          setPermission({
            btEdit: ['view'],
            btDelete: ['view'],
          });
        } else {
          setPermission({
            btEdit: ['view'],
            btDelete: ['view', 'edit'],
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  const getAuth = () => {
    return new Promise((resolve, reject) => {
      try {
        let auth = AsyncStorage.getItem('auth');
        let position = AsyncStorage.getItem('position');
        resolve({position: position, auth: auth});
      } catch (error) {
        console.error(error);
        reject(error);
      }
    });
  };
  return [permission, fetch];
};

export const PermissionCtx = React.createContext({});
const PermissionProvider = ({children}) => {
  const [permission, fetch] = usePermission();
  return (
    <PermissionCtx.Provider
      value={{
        permission: permission,
        fetch: fetch,
      }}>
      {children}
    </PermissionCtx.Provider>
  );
};

export default PermissionProvider;
