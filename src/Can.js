import React from 'react';
import {PermissionCtx} from './permission.context';

export const Can = props => {
  const value = React.useContext(PermissionCtx);
  if (value.permission !== undefined) {
    const objectRef = props.objectRef;
    const access = props.access;
    const permission = value.permission;
    const perm = permission[objectRef];
    for (let i = 0; i < access.length; i++) {
      const found = perm.indexOf(access[i]);
      if (found >= 0) {
        return props.children;
      }
    }
  }

  return null;
};

// Can.defaultProps = {
//     access: ["view", "edit"]
// }
