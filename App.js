import React, {Component} from 'react';
import {Provider, connect} from 'react-redux';
import {createStore, combineReducers} from 'redux';
import PLogin from './src/PLogin';
import PHome from './src/PHome';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import PermissionProvider from './src/permission.context';

let StaticPositionContainer = connect(state => ({
  position: state.RPosition.position,
  permission: state.RPermission.permission,
}))(PHome);

const AppNavigator = createSwitchNavigator({
  PLogin: PLogin,
  PHome: StaticPositionContainer,
});

const AppContainer = createAppContainer(AppNavigator);

const initialState = {
  position: '',
  permission: {},
};

const reducerPosition = (state = initialState, action) => {
  switch (action.type) {
    case 'POSITION': {
      return {position: action.position};
    }
  }
  return state;
};

const reducerPermission = (state = initialState, action) => {
  switch (action.type) {
    case 'PERMISSION': {
      return {permission: action.permission};
    }
  }
  return state;
};

const store = createStore(
  combineReducers({
    RPosition: reducerPosition,
    RPermission: reducerPermission,
  }),
);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PermissionProvider>
          <AppContainer />
        </PermissionProvider>
      </Provider>
    );
  }
}
